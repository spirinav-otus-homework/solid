﻿using System;

using Solid.Interfaces;

namespace Solid.Validators
{
    public class GameSettingsValidator : IGameSettingsValidator
    {
        public void Validate(IGameSettings gameSettings)
        {
            if (gameSettings == null)
            {
                throw new ArgumentException("The game settings were not initialized.");
            }

            if (gameSettings.MaxNum <= gameSettings.MinNum)
            {
                throw new ArgumentException("MaxNum should be greater than MinNum.");
            }

            if (gameSettings.AttemptNum <= 0)
            {
                throw new ArgumentException("AttemptNum should be greater than zero.");
            }
        }
    }
}