﻿namespace Solid.Interfaces
{
    public interface IGameSettingsValidator
    {
        void Validate(IGameSettings gameSettings);
    }
}