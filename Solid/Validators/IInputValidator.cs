﻿using Solid.Results;

namespace Solid.Interfaces
{
    public interface IInputValidator
    {
        ValidationResult Validate(string input);
    }
}