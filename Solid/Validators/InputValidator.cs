﻿using Solid.Interfaces;
using Solid.Results;

namespace Solid.Validators
{
    public class InputValidator : IInputValidator
    {
        private readonly IGameSettings _gameSettings;
        private int _num;

        public InputValidator(IGameSettings gameSettings)
        {
            _gameSettings = gameSettings;
        }

        public ValidationResult Validate(string num)
        {
            if (!int.TryParse(num, out _num) || _num < _gameSettings.MinNum || _num > _gameSettings.MaxNum)
            {
                return new ErrorResult(
                    $"You should enter a number from {_gameSettings.MinNum} to {_gameSettings.MaxNum}. Try again.");
            }

            return new OkResult(_num);
        }
    }
}