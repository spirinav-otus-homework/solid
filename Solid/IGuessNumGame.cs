﻿namespace Solid.Interfaces
{
    public interface IGuessNumGame
    {
        void Play();
    }
}