﻿using Solid.Interfaces;

namespace Solid.Settings
{
    public class GameSettings : IGameSettings
    {
        public int MinNum { get; set; }

        public int MaxNum { get; set; }

        public int AttemptNum { get; set; }
    }
}