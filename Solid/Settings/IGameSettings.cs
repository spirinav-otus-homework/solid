﻿namespace Solid.Interfaces
{
    public interface IGameSettings
    {
        int MinNum { get; }

        int MaxNum { get; }

        int AttemptNum { get; }
    }
}