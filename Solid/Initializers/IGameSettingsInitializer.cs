﻿namespace Solid.Interfaces
{
    public interface IGameSettingsInitializer
    {
        IGameSettings Init();
    }
}