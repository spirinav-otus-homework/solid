﻿using System.IO;

using Microsoft.Extensions.Configuration;

using Solid.Interfaces;
using Solid.Settings;

namespace Solid.Initializers
{
    public class JsonGameSettingsInitializer : IGameSettingsInitializer
    {
        private readonly IGameSettingsValidator _validator;

        public JsonGameSettingsInitializer(IGameSettingsValidator validator)
        {
            _validator = validator;
        }

        public IGameSettings Init()
        {
            var gameSettings = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build()
                .GetSection("GameSettings")
                .Get<GameSettings>();

            _validator.Validate(gameSettings);

            return gameSettings;
        }
    }
}