﻿namespace Solid.Results
{
    public class ErrorResult : ValidationResult
    {
        private readonly string _message;

        public ErrorResult(string message)
        {
            _message = message;
        }

        public override bool Valid => false;

        public override int ValidatedNum => default;

        public override bool Continue => true;

        public override string GetMessage() => _message;
    }
}