﻿namespace Solid.Results
{
    public class OkResult : ValidationResult
    {
        public OkResult(int validatedNum)
        {
            ValidatedNum = validatedNum;
        }

        public override bool Valid => true;

        public override int ValidatedNum { get; }

        public override bool Continue => true;

        public override string GetMessage() => string.Empty;
    }
}