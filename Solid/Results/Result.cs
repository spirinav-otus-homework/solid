﻿namespace Solid.Results
{
    public abstract class Result
    {
        public abstract bool Continue { get; }

        public abstract string GetMessage();
    }
}