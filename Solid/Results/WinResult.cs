﻿namespace Solid.Results
{
    public class WinResult : Result
    {
        public override bool Continue => false;

        public override string GetMessage() => "Congratulations! You guessed the number!";
    }
}