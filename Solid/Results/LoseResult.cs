﻿namespace Solid.Results
{
    public class LoseResult : Result
    {
        private readonly int _targetNum;

        public LoseResult(int targetNum)
        {
            _targetNum = targetNum;
        }

        public override bool Continue => false;

        public override string GetMessage() => $"Unfortunately, you lost. The hidden number is {_targetNum}.";
    }
}