﻿namespace Solid.Results
{
    public class GreaterResult : Result
    {
        private readonly int _num;

        public GreaterResult(int num)
        {
            _num = num;
        }

        public override bool Continue => true;

        public override string GetMessage() => $"The hidden number is greater than {_num}. Try again.";
    }
}