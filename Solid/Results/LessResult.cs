﻿namespace Solid.Results
{
    public class LessResult : Result
    {
        private readonly int _num;

        public LessResult(int num)
        {
            _num = num;
        }

        public override bool Continue => true;

        public override string GetMessage() => $"The hidden number is less than {_num}. Try again.";
    }
}