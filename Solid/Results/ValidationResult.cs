﻿namespace Solid.Results
{
    public abstract class ValidationResult : Result
    {
        public abstract bool Valid { get; }

        public abstract int ValidatedNum { get; }
    }
}