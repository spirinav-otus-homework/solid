﻿using Solid.Interfaces;
using Solid.Results;

namespace Solid.Comparers
{
    public class NumComparer : INumComparer
    {
        private readonly IGameSettings _gameSettings;

        public NumComparer(IGameSettings gameSettings)
        {
            _gameSettings = gameSettings;
        }

        public Result Compare(int targetNum, int inputNum, int attemptNum)
        {
            if (targetNum == inputNum)
            {
                return new WinResult();
            }

            if (attemptNum == _gameSettings.AttemptNum)
            {
                return new LoseResult(targetNum);
            }

            if (targetNum < inputNum)
            {
                return new LessResult(inputNum);
            }

            return new GreaterResult(inputNum);
        }
    }
}