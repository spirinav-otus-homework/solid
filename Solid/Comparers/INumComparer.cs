﻿using Solid.Results;

namespace Solid.Interfaces
{
    public interface INumComparer
    {
        Result Compare(int targetNum, int inputNum, int attemptNum);
    }
}