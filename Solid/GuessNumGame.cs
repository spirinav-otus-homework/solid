﻿using Solid.Interfaces;
using Solid.Results;

namespace Solid
{
    public class GuessNumGame : IGuessNumGame
    {
        private readonly IGameSettings _settings;
        private readonly INumGenerator _generator;
        private readonly IWriter _writer;
        private readonly IReader _reader;
        private readonly IInputValidator _validator;
        private readonly INumComparer _comparer;

        private int _targetNum;
        private int _attemptCount;
        private Result _result;

        public GuessNumGame(IGameSettings settings, INumGenerator generator, IWriter writer, IReader reader,
            IInputValidator validator, INumComparer comparer)
        {
            _settings = settings;
            _generator = generator;
            _writer = writer;
            _reader = reader;
            _validator = validator;
            _comparer = comparer;
        }

        public void Play()
        {
            _targetNum = _generator.Generate();
            _writer.Write($"Guess the number from {_settings.MinNum} to {_settings.MaxNum}. " +
                          $"You have {_settings.AttemptNum} attempts. Good luck!");

            _attemptCount = 1;
            do
            {
                _writer.Write($"Attempt № {_attemptCount}:");
                var validationResult = _validator.Validate(_reader.Read());
                _result = !validationResult.Valid
                    ? validationResult
                    : _comparer.Compare(_targetNum, validationResult.ValidatedNum, _attemptCount++);
                _writer.Write(_result.GetMessage());
            } while (_result?.Continue ?? false);
        }
    }
}