﻿using Microsoft.Extensions.DependencyInjection;

using Solid.Comparers;
using Solid.Generators;
using Solid.Interfaces;
using Solid.IO;
using Solid.Initializers;
using Solid.Validators;

namespace Solid
{
    internal class Program
    {
        private static void Main()
        {
            new ServiceCollection()
                .AddSingleton(new JsonGameSettingsInitializer(new GameSettingsValidator()).Init())
                .AddTransient<INumGenerator, NumGenerator>()
                .AddTransient<IReader, DataConsole>()
                .AddTransient<IWriter, DataConsole>()
                .AddTransient<IInputValidator, InputValidator>()
                .AddTransient<INumComparer, NumComparer>()
                .AddSingleton<IGuessNumGame, GuessNumGame>()
                .BuildServiceProvider()
                .GetService<IGuessNumGame>()?.Play();
        }
    }
}