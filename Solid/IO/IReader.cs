﻿namespace Solid.Interfaces
{
    public interface IReader
    {
        string Read();
    }
}