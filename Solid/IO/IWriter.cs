﻿namespace Solid.Interfaces
{
    public interface IWriter
    {
        void Write(string data);
    }
}