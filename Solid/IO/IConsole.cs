﻿namespace Solid.Interfaces
{
    public interface IConsole : IWriter, IReader
    {
    }
}