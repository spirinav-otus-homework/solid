﻿using System;

using Solid.Interfaces;

namespace Solid.IO
{
    public class DataConsole : IConsole
    {
        public void Write(string data) => Console.WriteLine(data);

        public string Read() => Console.ReadLine();
    }
}