﻿namespace Solid.Interfaces
{
    public interface INumGenerator
    {
        int Generate();
    }
}