﻿using System;

using Solid.Interfaces;

namespace Solid.Generators
{
    public class NumGenerator : INumGenerator
    {
        private readonly Random _random = new();

        private readonly IGameSettings _gameSettings;

        public NumGenerator(IGameSettings gameSettings)
        {
            _gameSettings = gameSettings;
        }

        public int Generate()
        {
            return _random.Next(_gameSettings.MinNum, _gameSettings.MaxNum);
        }
    }
}